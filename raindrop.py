import sys
import itertools
import pygame
import shapes

rain_file = './sprites/rain.png'
rain_image = pygame.image.load(rain_file)

rain_splatter_file = './sprites/rain_splatter.png'
rain_splatter_image = pygame.image.load(rain_splatter_file)

class RainDrop(object):
    def __init__(self, x):
        self.x = x
        self.y = 0
        self.finished = False
        self.splattered = False
        self.splattered_time = 0

    def step(self):
        if not self.splattered:
            self.y = self.y + 8
            if self.y > 700:
                self.splattered = True
        else:
            self.splattered_time = self.splattered_time + 1
            if self.splattered_time > 25:
                self.finished = True

    def image(self):
        if self.splattered:
            return rain_splatter_image
        else:
            return rain_image

    def rect(self):
        rect = self.image().get_rect()
        rect.centerx = self.x
        rect.bottom = self.y
        return rect
