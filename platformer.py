import sys
import itertools
import pygame
import random

pygame.init()

import game_loop
import player
import shapes
import raindrop

p1 = player.Player()
rain_drops = set()

x_file = './sprites/x.png'
x_image = pygame.image.load(x_file)

heart_file = './sprites/heart.png'
heart_image = pygame.image.load(heart_file)

def world_to_screen(p):
    x, y = p
    return (x, 700 - y)

class Platformer(object):
    def __init__(self):
        self.running = True

    def handle_tick(self):
        if self.running:
            keys = pygame.key.get_pressed()

            left_pressed = keys[pygame.K_LEFT] == 1
            right_pressed = keys[pygame.K_RIGHT] == 1
            up_pressed = keys[pygame.K_UP] == 1
            down_pressed = keys[pygame.K_DOWN] == 1

            go_left = left_pressed and not right_pressed
            go_right = right_pressed and not left_pressed
            go_up = up_pressed and not down_pressed
            go_down = down_pressed and not up_pressed

            p1.step(go_up, go_down, go_left, go_right)

            if random.randint(0, 20) == 20:
                rain_drops.add(raindrop.RainDrop(random.randint(0, 700)))

            removed = set()
            for drop in rain_drops:
                drop.step()
                if not drop.splattered:
                    if drop.rect().colliderect(p1.rect()):
                        p1.lose_life()
                        if p1.dead:
                            self.running = False
                        removed.add(drop)
                if drop.finished:
                    removed.add(drop)
            rain_drops.difference_update(removed)

    def handle_event(self, event):
        pass

    def draw(self, screen):
        screen.fill((0,0,0))

        if self.running:
            for drop in rain_drops:
                image = drop.image()
                rect = drop.rect()
                screen.blit(image, rect)

            # Render player
            rect = p1.rect()
            screen.blit(p1.image, rect)

            for i in range(p1.lives):
                rect = heart_image.get_rect()
                rect.top = 5
                rect.left = ((rect.width + 5) * i) + 5
                screen.blit(heart_image, rect)
        else:
            rect = x_image.get_rect(center=(350, 350))
            screen.blit(x_image, rect)

game_loop.GameLoop(Platformer()).run()
